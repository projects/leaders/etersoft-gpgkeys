#!/bin/sh

USERNAME=$1

tempkey=
cleanup_handler()
{
        trap - EXIT
        [ -z "$tempkey" ] || rm -f "$tempkey"
        exit "$@"
}

exit_handler()
{
        cleanup_handler $?
}

signal_handler()
{
        cleanup_handler 143
}

docmd()
{
        echo "\$ $@"
        "$@"
}

fatal()
{
        echo "Error: $@"
        exit 1
}

[ -n "$USERNAME" ] || fatal "Usage: $0 username"

trap exit_handler EXIT
trap signal_handler HUP PIPE INT QUIT TERM
tempkey="$(mktemp -t tempkey.XXXXXX)"

USERHOME=$(getent passwd $USERNAME | cut -d: -f6)
HOMEDIR="$USERHOME/.gnupg"

export LANG=C

sudo gpg --list-keys --homedir=$HOMEDIR
sudo gpg --export --armor --homedir=$HOMEDIR >$tempkey || fatal "Can't export gpg key"
sudo chown $USER $tempkey || fatal

cp $tempkey eterkeys/$USERNAME || fatal
./alt-gpgkey-strip eterkeys/$USERNAME || fatal "Can't strip"
git add eterkeys/$USERNAME
echo "Key for $USERNAME added to git. Please, commit now"
